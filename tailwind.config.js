module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      '15': '0.15'
    },
  },
  variants: {
    extend: {
      backgroundOpacity: ['active'],
    },
  },
  plugins: [],
}
