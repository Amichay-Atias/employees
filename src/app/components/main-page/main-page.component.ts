import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from 'src/app/class/employee';
import { EmployeeService } from 'src/app/services/employee.service';
import { TransparentDataService } from 'src/app/services/transparent-data/transparent-data.service';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {

  public employees: Employee[] = [];
  public loadingData: boolean = false;

  constructor(private employeeService: EmployeeService,
    private transparentDataService: TransparentDataService,
    private router: Router) { }

  ngOnInit() {
    this.getDataFromServer();
  }

  //get emoployees list
  getDataFromServer() {
    this.employeeService.getEmployees().subscribe((res) => {
      this.employees = [];
      setTimeout(() => {
        this.loadingData = true;
      }, 500);

      if (res) {
        for (let index of Object.keys(res)) {
          res[index]["userIndex"] = index;
          res[index]["selected"] = false;
          this.employees.push(res[index]);
        }
      }
      else {
        //failed loading data
      }
    })
  }

  //edit
  public editEmployee(employee: Employee) {
    this.transparentDataService.saveEmployeeState = true;
    this.transparentDataService.employee = employee;

    this.router.navigateByUrl('/edit-employee');
  }



  //delete
  deleteEmployees() {
    let indexsToDelete = "";

    for (let employee of this.employees) {
      if (employee.selected)
        indexsToDelete += employee.userIndex + ",";
    }
    indexsToDelete = indexsToDelete.slice(0, -1);

    this.employeeService.deleteEmployee(indexsToDelete).subscribe((res) => {
      if (res) {

        //refresh data 
        this.getDataFromServer();
      }
      else {
        //failed delte data

      }
    });
  }



}
