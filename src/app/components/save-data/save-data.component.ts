import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from 'src/app/class/employee';
import { EmployeeService } from 'src/app/services/employee.service';
import { TransparentDataService } from 'src/app/services/transparent-data/transparent-data.service';

@Component({
  selector: 'app-save-data',
  templateUrl: './save-data.component.html',
  styleUrls: ['./save-data.component.scss']
})
export class SaveDataComponent implements OnInit {

  public employee: Employee = {
    fullName: "",
    idNumber: 0,
    sex: "",
    comments: "",
    userIndex: 0,
    selected: 0
  };
  public errorMessage: string = "";
  public editState = false;

  constructor(private employeeService: EmployeeService,
    private router: Router,
    private transparentDataService: TransparentDataService) { }

  ngOnInit() {
    //in case of edit
    if (this.transparentDataService.saveEmployeeState) {
      this.editState = true;
      this.employee = this.transparentDataService.employee;
    }
  }

  clerEmployeeData() {
    this.employee = {
      fullName: "",
      idNumber: 0,
      sex: "",
      comments: ""
    };
  }

  saveData() {
    if (!this.employee.fullName || this.employee.fullName == "") {
      alert("Please fill user name");
      return;
    }
    this.employee.sex = this.employee.sex ? "female" : "male";

    //edit
    if (this.editState) {
      this.editEmployee();
    }

    else {
      this.addEmployee();
    }
  }

  //send request to add employee
  addEmployee() {
    this.employeeService.addEmployee(this.employee).subscribe((res) => {

      if (res) {
        this.clerEmployeeData();

        this.router.navigateByUrl('/home-page');
      }
      else {

        this.errorMessage = "Failed try again please";
      }
    });

  }


  editEmployee() {
    this.employeeService.editEmployee(this.employee, this.employee.userIndex).subscribe((res) => {
      if (res) {
        this.clerEmployeeData();
        this.transparentDataService.clerData();
        this.transparentDataService.saveEmployeeState = false;

        this.router.navigateByUrl('/home-page');
      }
      else {

        this.errorMessage = "Failed try again please";
      }
    });
  }
}
