export interface Employee {
    fullName: string,
    idNumber: number,
    sex: string,
    comments: string,

    userIndex?: any
    selected?: any
}
