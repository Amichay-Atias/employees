import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainPageComponent } from './components/main-page/main-page.component';
import { SaveDataComponent } from './components/save-data/save-data.component';

const routes: Routes = [
  {
    path: "",
    component: MainPageComponent
  },
  {
    path: "home-page",
    component: MainPageComponent
  },
  {
    path: "add-employee",
    component: SaveDataComponent
  },
  {
    path: "edit-employee",
    component: SaveDataComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
