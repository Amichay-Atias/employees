import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Employee } from '../class/employee';
import { DataService } from './data/data.service';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private dataService: DataService) { }


  public getEmployees(): Observable<any> {
    this.dataService.apiName = "get-employees";
    return this.dataService.getData();
  }

  public addEmployee(employee: Employee): Observable<any> {
    this.dataService.apiName = "add-employee";
    return this.dataService.saveData(employee);
  }

  public editEmployee(employee: Employee, employeeIndex: number): Observable<any> {
    this.dataService.apiName = "edit-employee";
    return this.dataService.updateData(employeeIndex, employee);
  }

  public deleteEmployee(employeesIndex: string): Observable<any> {
    this.dataService.apiName = "delete-employees";
    return this.dataService.deleteData({employeesIndex: employeesIndex});
  }

}
