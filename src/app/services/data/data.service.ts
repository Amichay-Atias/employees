import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private baseUrl: string;
  public apiName: string;

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {
    this.baseUrl = "http://localhost:3000/api/";
    this.apiName = "";
  }

  // to get data
  public getData(): Observable<any> {
    return this.http.get(this.baseUrl + this.apiName);
  }

  // save data
  public saveData(data: any): Observable<any> {
    return this.http.post(this.baseUrl + this.apiName, JSON.stringify(data), this.httpOptions);
  }

  // update data
  public updateData(paramValue: string | number, data: any): Observable<any> {
    return this.http.post(this.baseUrl + this.apiName + '/' + paramValue, JSON.stringify(data), this.httpOptions);
  }

  //delete data
  public deleteData(data: any): Observable<any> {
    return this.http.post(this.baseUrl + this.apiName, JSON.stringify(data), this.httpOptions);
  }
}
