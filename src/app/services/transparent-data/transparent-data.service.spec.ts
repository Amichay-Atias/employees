/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { TransparentDataService } from './transparent-data.service';

describe('Service: TransparentData', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TransparentDataService]
    });
  });

  it('should ...', inject([TransparentDataService], (service: TransparentDataService) => {
    expect(service).toBeTruthy();
  }));
});
