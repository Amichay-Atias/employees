import { Injectable } from '@angular/core';
import { Employee } from 'src/app/class/employee';

@Injectable({
  providedIn: 'root'
})
export class TransparentDataService {

  constructor() { }
  public saveEmployeeState: boolean = false;

  public employee: Employee = {
    fullName: "",
    idNumber: 0,
    sex: "",
    comments: "",

    userIndex: 0,
    selected: null
  }

  public clerData() {
    this.employee = {
      fullName: "",
      idNumber: 0,
      sex: "",
      comments: "",

      userIndex: 0,
      selected: null
    }
  }
}
